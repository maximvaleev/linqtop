﻿
namespace LinqTop;

internal static class MyExtensions
{
    public static IEnumerable<TSource> Top<TSource> (this IEnumerable<TSource> source, int percent)
        where TSource : IComparable<TSource>
    {
        return source.Top(percent, x => x);
    }

    public static IEnumerable<TSource> Top<TSource,TKey>(this IEnumerable<TSource> source,
                                                         int percent,
                                                         Func<TSource, TKey> keySelector)
        where TKey : IComparable<TKey>
    {
        if (source is null)
        {
            throw new ArgumentNullException(nameof(source));
        }
        if (percent > 100 || percent < 1)
        {
            throw new ArgumentOutOfRangeException(nameof(percent), percent, "Значение процента должно быть от 1 до 100.");
        }

        int elementsToReturn = (int)Math.Ceiling(source.Count() * percent / 100d);
        return source.OrderByDescending(keySelector).Take(elementsToReturn);
    }
}
